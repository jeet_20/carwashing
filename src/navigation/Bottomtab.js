import React from 'react';
import {Image} from 'react-native';
import homes from'../screens/homes/index';
import help from'../screens/help/index';
import sos from'../screens/sos/index';
import mycars from'../screens/mycars/index';
import accounts from'../screens/account/index';
import {createBottomTabNavigator}from'@react-navigation/bottom-tabs';
const Tab1=createBottomTabNavigator();
const Bottomtab=()=>{
    return(
        <Tab1.Navigator>
            <Tab1.Screen name="homes"component={homes}options={{
            tabBarLabel: 'Home',
            tabBarIcon: (focused, tintColor) => (
              <Image style={{ width: 30, height: 30 }} 
                     source={require('../assets/home.png')} />
            )
            }}/>
            <Tab1.Screen name="help"component={help}options={{
            tabBarLabel: 'Help',
            tabBarIcon:()=>(
              <Image source={require('../assets/help.png')} style={{height:30,width:30}}>

              </Image>

            )}}/>
              <Tab1.Screen name="sos"component={sos}options={{
            tabBarLabel: 'SOS',
            tabBarIcon:()=>(
              <Image source={require('../assets/persons.png')} style={{height:30,width:30}}>
                </Image>
            )}}/>
              <Tab1.Screen name="mycars"component={mycars}options={{
            tabBarLabel: 'MYCar',
     tabBarIcon:()=>(
    <Image source={require('../assets/cars.png')} style={{height:30,width:30}}></Image> )}}/>
              <Tab1.Screen name="accounts"component={accounts}options={{
            tabBarLabel: 'Account',
    tabBarIcon:()=>(
              <Image source={require('../assets/account.jpg')} style={{height:30,width:30}}>
                </Image>)}}/>
        </Tab1.Navigator>
    )
}
export default Bottomtab;