import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Splash from '../screens/splash';
import Login from '../screens/login';
import mobile from '../screens/mobile';
import otp from  '../screens/otp';
import Home from '../screens/Home';
import map from '../screens/map';
import account from '../screens/account';
import Bottomtab from'../navigation/Bottomtab';
const Stack = createStackNavigator();
const MainStack=()=>{
    return(
        <Stack.Navigator>
        <Stack.Screen name="Splash" component={Splash} options={{headerShown:false}}/>
        <Stack.Screen name="Login" component={Login}  options={{headerShown:false}}/>
        <Stack.Screen name="mobile" component={mobile} options={{headerShown:false}} />
        <Stack.Screen name="Home" component={Home} options={{headerShown:false}} />
        <Stack.Screen name="otp" component={otp} options={{headerShown:false}}/>
        <Stack.Screen name="map" component={map}options={{headerShown:false}}/>
        <Stack.Screen name="Bottomtab"component={Bottomtab} options={{headerShown:false}} />
        <Stack.Screen name="account" component={account}/>
      </Stack.Navigator>
    )
}
export default MainStack;