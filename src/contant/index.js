import React from 'react';
import {View,Text} from 'react-native';
const Header =({props})=>{
    return(
        <View style={{marginLeft:'5%'}}>
            <Text>{props}</Text>
        </View>
    )
}
export default Header;