import React, { useEffect, useState } from 'react'
import { View, Text, Alert, TextInput, TouchableOpacity, Image, ScrollView, KeyboardAvoidingView } from 'react-native'
import Geolocation from '@react-native-community/geolocation';
import MapView, { Marker } from 'react-native-maps';

const Home = ({navigation}) => {  
  const [state, setState] = useState({
    region: {
      latitude: 0,
      longitude: 0,
      latitudeDelta: 0.97,
      longitudeDelta: 0.98,
    }
  })

  useEffect(() => {
    Geolocation.getCurrentPosition(
      position => {
        setState({
          region: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: 0.97,
            longitudeDelta: 0.98,
          }
        });
      },
      (error) => {
        Alert.alert(error.message.toString());
      },
      {
        showLocationDialog: true,
        enableHighAccuracy: true,
        timeout: 20000
      },
    );

  });
  return (
    <View style={{ flex: 1, height: '50%', backgroundColor: '#ffe0cc' }}>
      <MapView
        style={{ height: '50%' }}
        showsUserLocation={true}
        region={state.region}
        onRegionChange={() => state.region}
      >
        <Marker
          coordinate={state.region}
        />
      </MapView>
      <View>
        <View style={{
          flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center', marginLeft: '3%', padding: 15
        }}>
          <TouchableOpacity  onPress={()=>Account()}>
            <View style={{ borderWidth: 2, padding: 10, backgroundColor: 'white', borderRadius: 10 }}>
              <Image source={require('../../assets/swift.jpg')} style={{ height: 30, width: 40 }}></Image>
              <Text>swift</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity>
            <View style={{ borderWidth: 2, padding: 10, backgroundColor: 'white', borderRadius: 10 }}>
              <Image source={require('../../assets/for.png')} style={{ height: 30, width: 40 }}></Image>
              <Text>forturner</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity>
            <View style={{ borderWidth: 2, padding: 10, backgroundColor: 'white', borderRadius: 10 }}>
              <Image source={require('../../assets/baleno.png')} style={{ height: 30, width: 40 }}></Image>
              <Text>baleno</Text>
            </View>
          </TouchableOpacity>
        </View>
        <View style={{ height: '30%', padding: 30 }}>
          <ScrollView>
            <View style={{ borderBottomWidth: 2, }}>
              <TextInput placeholder='Pickup location'></TextInput>
            </View>
            <View style={{ borderBottomWidth: 2, }}>
              <TextInput placeholder='Destination'></TextInput>
            </View>
            <View style={{ borderBottomWidth: 2, }}>
              <TextInput placeholder='Offer your fare'></TextInput>
            </View>
            <View style={{ borderBottomWidth: 2, }}>
              <TextInput placeholder='Comment and wishes'></TextInput>
            </View>
          </ScrollView>
        </View>
        <TouchableOpacity onPress={()=>navigation.navigate('Bottomtab')}>
          <View style={{
            justifyContent: 'center', alignItems: "center",
            borderWidth: 2, marginLeft: '25%', marginRight: '25%', borderRadius: 10,
            backgroundColor: '#ff9999', padding: 6, marginTop: '10%'
          }}>
            <Text style={{ fontSize: 20, color: 'black' }}>Next</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  )
}
export default Home;