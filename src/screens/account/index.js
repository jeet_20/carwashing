import React from 'react';
import { TextInput, TextInputBase, View, Image, Text } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
const Account = () => {
    return (
        <View>

            <View style={{marginLeft:'5%' ,marginTop:'2%'}}>
                <Text style={{ fontSize: 20, }}>MY Account</Text>
            </View>
            <View style={{ marginTop: '2%',borderRadius:20,backgroundColor:'white' ,width:'90%',height:'92%',marginLeft:'5%'}}>
                <View style={{
                    flexDirection: 'row', marginLeft: '5%', marginTop: '10%', borderBottomWidth: 2, borderBottomColor: 'grey'
                    , margin: '5%'
                }}>
                    <Image source={require('../../assets/account.jpg')} style={{ height: 30, width: 30 }}></Image>
                    <Text style={{ marginLeft: '5%' }}>MY cars</Text>
                </View>
                <View style={{
                    flexDirection: 'row', marginLeft: '5%', marginTop: '2%', borderBottomWidth: 2, borderBottomColor: 'grey'
                    , margin: '5%'
                }}>
                    <Image source={require('../../assets/key.png')} style={{ height: 30, width: 30 }}></Image>
                    <Text style={{ marginLeft: '5%' }}>MY Profile</Text>
                </View>
                <View style={{
                    flexDirection: 'row', marginLeft: '5%', marginTop: '2%', borderBottomWidth: 2, borderBottomColor: 'grey'
                    , margin: '5%'
                }}>
                    <Image source={require('../../assets/gift.png')} style={{ height: 30, width: 30 }}></Image>
                    <Text style={{ marginLeft: '5%' }}>Refer And Earn</Text>
                </View>
                <View style={{
                    flexDirection: 'row', marginLeft: '5%', marginTop: '2%', borderBottomWidth: 2, borderBottomColor: 'grey'
                    , margin: '5%'
                }}>
                    <Image source={require('../../assets/help.png')} style={{ height: 30, width: 30 }}></Image>
                    <Text style={{ marginLeft: '5%' }}>Order History </Text>
                </View>
                <View style={{ flexDirection: 'row',margin:'5%', marginLeft: '5%', marginTop: '2%', borderBottomWidth: 2, borderBottomColor: 'grey' }}>
                    <Image source={require('../../assets/share.png')} style={{ height: 30, width: 30 }}></Image>
                    <Text style={{ marginLeft: '5%' }}>Help & Support </Text>
                </View>
                <View style={{
                    flexDirection: 'row', marginLeft: '5%', marginTop: '2%', borderBottomWidth: 2, borderBottomColor: 'grey',
                    margin: '5%' }}>
                    <Image source={require('../../assets/log.jpg')} style={{ height: 30, width: 30 }}></Image>
                    <Text style={{ marginLeft: '5%' }}>Log Out </Text>
                </View>
                <TouchableOpacity>
                <View style={{justifyContent:'center',alignItems:'center',margin:'5%'}}>
                    <Text style={{ marginLeft: '5%', textDecorationLine: 'underline',
                    fontSize:15}}> Terms & Condition |Privacy Policy </Text>
                </View>
            </TouchableOpacity>

            </View>
        </View>


    )
}
export default Account;