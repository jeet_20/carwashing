import React from 'react';
import { Text, View, Image } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
// import Mobile from '../mobile';
const Login = ({ navigation }) => {
    const Mobile = () => {
        navigation.navigate('mobile')
    }
    return (
        <View style={{ margin: '10%' }}>
            <TouchableOpacity>
                <View style={{ marginTop: '10%', justifyContent: 'center', alignItems: 'center', borderWidth: 2, backgroundColor: '#f194ff' }}>
                    <Text style={{ fontSize: 20 }}>Login With Email </Text>
                </View>
            </TouchableOpacity>
            <TouchableOpacity>
                <View style={{ marginTop: '10%', justifyContent: 'center', alignItems: 'center', borderWidth: 2, backgroundColor: '#f194ff' }}>
                    <Text style={{ fontSize: 20 }}>Login With Facebook </Text>
                </View>
            </TouchableOpacity>
            <TouchableOpacity>
                <View style={{ marginTop: '10%', justifyContent: 'center', alignItems: 'center', borderWidth: 2, backgroundColor: '#f194ff' }}>
                    <Text style={{ fontSize: 20 }}>Login With Twitter </Text>
                </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => Mobile()}>
                <View style={{ marginTop: '10%', justifyContent: 'center', alignItems: 'center', borderWidth: 2, backgroundColor: '#f194ff' }}>
                    <Text style={{ fontSize: 20 }}>Login With Mobile</Text>
                </View>
            </TouchableOpacity>
            <View>
                <Image source={require('../../assets/fb.png')}
                    style={{ height: 40, width: 40,marginTop:'90%' }}></Image>
            </View>
        </View>
    )
}
export default Login;