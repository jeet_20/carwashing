import React,{useState} from 'react';
import { Text, TouchableOpacity, View, Image, TextInput, FlatList, ScrollView, Alert } from 'react-native';
import MainHeader from '../../contant/index';
const Home = ({ navigation }) => {
    const [carBrand,setCarBrand]= useState([
    {image:require('../../assets/maruti.png')},
    {image:require('../../assets/audi.png')},
    {image:require('../../assets/bhugatti.png')},
    {image:require('../../assets/landrover.jpg')},
     {image:require('../../assets/datsun.jpg')},
    {image:require('../../assets/ford.jpg')},
    {image:require('../../assets/jagvar.jpg')},
    {image:require('../../assets/mahindra.png')},
    {image:require('../../assets/nissan.jpg')},
    {image:require('../../assets/tata.png')},
    {image:require('../../assets/toyoto.png')}
])
const nextpage=()=>{
    navigation.navigate('map')
}
    return (
        <>
        <View style={{padding:5,marginTop:'5%',}}>
            <MainHeader props="Select Your Car"/>
            <ScrollView>
                <View>
            <View style={{backgroundColor:'white',marginTop:'5%',marginLeft:'5%',marginRight:'5%',borderRadius:10}}>
              <TextInput placeholder ='search by car model or brand' placeholderTextColor='black'></TextInput>  
            </View>
            <ScrollView horizontal={true}indicatorStyle={false}>
            <FlatList
            data={carBrand}
            numColumns={3}
            renderItem={({item,index})=>(
                <View key={index} style={{marginTop:'2%'}}>
                    <TouchableOpacity onPress={()=>alert(index.valueOf())}>
                    <Image source={item.image} style={{height:100,width:100,margin:5,borderRadius:30}} resizeMode="contain"></Image>
                    </TouchableOpacity>
                </View>
            )}
            >
            </FlatList>
            </ScrollView>
            <TouchableOpacity onPress={()=>nextpage()}>
            <View style={{backgroundColor:'red',padding:7,borderRadius:20}}>
                <Text style={{alignSelf:'center',color:'white',fontSize:20,fontWeight:'bold'}}>Select</Text>
            </View>
            </TouchableOpacity>
            </View>
            </ScrollView>
        </View>
        </>
    )

}
export default Home;