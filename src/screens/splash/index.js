
import React from 'react';
import { Text, View, TouchableOpacity, ImageBackground, Image } from 'react-native';

const Splash = ({ navigation }) => {
    return (
        
        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <ImageBackground source={require('../../assets/water.png')} style={{ height: '100%', width: '100%', alignContent: 'center' }}>
                <View style={{ justifyContent: 'center', alignItems: 'center',marginLeft:'10%',marginRight:'10%',justifyContent:'center',alignItems:'center',}}>
                    <Image source={require('../../assets/carwash.png')} style={{ height: 80, width: 80, marginTop: '10%', padding: 5, justifyContent: 'center', alignItems: 'center'}} >
                    </Image>
                </View>

                <View style={{justifyContent:'center',alignItems:'center',marginTop:'20%'}}>
                    <Text style={{fontSize:30,color:'#990000'}}>car wash</Text>
                </View>            
                    <TouchableOpacity onPress={() => navigation.navigate('Login')}>
                    <View style={{marginTop:'90%',marginLeft:'10%',borderRadius:4
                    ,marginRight:'10%',justifyContent:'center',alignItems:'center',backgroundColor:'blue',padding:6}}>
                        <Text style={{fontSize:20,color:'white'}}>GET STARTED</Text>

                    </View>
                </TouchableOpacity>
            </ImageBackground>
        </View>
    )
}
export default Splash;